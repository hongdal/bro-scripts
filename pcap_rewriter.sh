#!/bin/bash 
#---------------------------------------------------------------------------------------------------
# This script is used for rewriting pcap files, such that the rewritten pcap file can be 
# replayed on to the network smoothly. 
#
# Note: all vlan tags in the packets are removed after rewritten. 
# 
# Hongda Li (hongdal@clemson.edu)
#---------------------------------------------------------------------------------------------------

if [[ -z $4 ]]; then
    echo "Usage: $0 <local interface> <new_ip> <bro's ip> <pcap file> [output file]"
    echo "+----------------------------------------------------------------------------------------+"
    echo "  local interface: Which intface you want to replay to."
    echo "  new ip: Specify an ip that does not appear in the pcap file. Change local ip to this ip."
    echo "  bro's ip: The ip of the host where Bro runs. "
    echo "  pcap file: The input file, captured by TCPdump. "
    echo "  output file: The output file, which will be used by TCPreplay. "
    echo "+-----------------------------------------------------------------------------------------+"
    exit 1
fi

intf=$1
new_ip=$2
broip=$3
file=$4
cachefile=$file".cache"
if [[ -z $5 ]]; then 
    output=$file".rewritten"
else
    output=$5
fi
if [[ ! -f $file ]]; then 
    echo "File $file not found!"
    exit 1
fi

# -- preparing ---
echo "Preparing ... "
local_ip=`ifconfig $intf | grep "inet addr" | cut -d ":" -f2 | cut -d ' ' -f1`
local_mac=`ifconfig $intf | grep "HWaddr" | awk '{print $5}'`
arping $broip -I $intf -f -w 10 > /dev/null
if [[ $? -ne 0 ]]; then
    echo "Could not find MAC for $broip. "
    exit 1
fi 
bro_mac=`arp -n | grep $broip | awk '{print $3}'`

# -- rewriting ---
echo "making cachefile ... "
tcpprep --auto=first -i $file -o $cachefile
if [[ $? -ne 0 ]]; then
    echo "tcpprep failed!"
    exit 1
fi 

echo "rewriting ... "
tcprewrite --enet-dmac=$bro_mac,$bro_mac --enet-smac=$local_mac,$local_mac \
           --srcipmap=$local_ip/32:$new_ip/32,$local_ip/32:$new_ip/32 \
           --dstipmap=$local_ip/32:$new_ip/32,$local_ip/32:$new_ip/32 \
           --enet-vlan=del \
           -i $file -c $cachefile -o $output 
if [[ $? -ne 0 ]]; then
    echo "tcprewrite failed!"
    exit 1
fi 


echo "+----------------------------------------------------+"
echo "|  SUCCESSFULLY REWROTE!                             |"
echo "+----------------------------------------------------+"
echo "  Summary:"
echo "  input file:      $file"
echo "  cache file:      $cachefile"
echo "  output file:     $output"
echo "  local ip:        $local_ip"
echo "  local MAC:       $local_mac"
echo "  destination ip:  $broip"
echo "  destination MAC: $bro_mac"
echo "  local ip is replace with: $new_ip"
echo "+----------------------------------------------------+"


