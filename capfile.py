#!/usr/bin/env python2
# coding=utf-8

import sys
import dpkt
import socket

def inet_to_str(inet):
    return socket.inet_ntop(socket.AF_INET, inet)

class capfile(object):

    def __init__(self, filename):
        self.clients = {
            "tcp": None,
            "udp": None,
            "icmp": None
        }
        self.servers = {
            "tcp": None,
            "udp": None,
            "icmp": None
        }
        self.connections = {
            "tcp": None,
            "udp": None,
            "icmp": None
        }
        self.name = filename

    def cap_printer(self, verbose = False):
        nonIP = 0;
        pcap_file = open(self.name)
        pcap = dpkt.pcap.Reader(pcap_file)
        for timestamp, buf in pcap:
            eth = dpkt.ethernet.Ethernet(buf)
            if eth.type != dpkt.ethernet.ETH_TYPE_IP:
                if verbose:
                    nonIP += 1;
                continue
            ip = eth.data
            print '%d,%lf\n' % (ip.len, timestamp)
        if verbose:
            print "------------------------------\n"
            print "NonIPV4 packets: %ld\n" % nonIP
        pcap_file.close()
   
    def tcp_classifier(self, verbose = False):
        self.clients["tcp"] = set()
        self.servers["tcp"] = set()
        self.connections["tcp"] = set()
        pcap_file = open(self.name)
        pcap = dpkt.pcap.Reader(pcap_file)
        for timestamp, buf in pcap:
            eth = dpkt.ethernet.Ethernet(buf)
            if eth.type != dpkt.ethernet.ETH_TYPE_IP:
                continue
            ip = eth.data
            if dpkt.ip.IP_PROTO_TCP != ip.p:
                continue;
            tcp = ip.data
            if not isinstance(tcp, dpkt.tcp.TCP):
                continue
            if tcp.sport >= 1024:
                if ip.src not in self.servers["tcp"]:
                    self.clients["tcp"].add(ip.src)
            else:
                if ip.src not in self.clients["tcp"]:
                    self.servers["tcp"].add(ip.src) 
            if tcp.dport >= 1024:
                if ip.dst not in self.servers["tcp"]:
                    self.clients["tcp"].add(ip.dst)
            else:
                if ip.dst not in self.clients["tcp"]:
                    self.servers["tcp"].add(ip.dst)
            self.connections["tcp"].add((ip.src, tcp.sport, ip.dst, tcp.dport))
        pcap_file.close()

    def udp_classifier(self, verbose = False):
        self.clients["udp"] = set()
        self.servers["udp"] = set()
        self.connections["udp"] = set()
        pcap_file = open(self.name)
        pcap = dpkt.pcap.Reader(pcap_file)
        for timestamp, buf in pcap:
            eth = dpkt.ethernet.Ethernet(buf)
            if eth.type != dpkt.ethernet.ETH_TYPE_IP:
                continue
            ip = eth.data
            if dpkt.ip.IP_PROTO_UDP != ip.p:
                continue;
            udp = ip.data
            if not isinstance(udp, dpkt.udp.UDP):
                continue
            if udp.sport >= 1024 :
                if ip.src not in self.servers["udp"]:
                    self.clients["udp"].add(ip.src)
            else:
                if ip.src not in self.clients["udp"]:
                    self.servers["udp"].add(ip.src)
            if udp.dport >= 1024:
                if ip.dst not in self.servers["udp"]:
                    self.clients["udp"].add(ip.dst)
            else:
                if ip.dst not in self.clients["udp"]:
                    self.servers["udp"].add(ip.dst)
            self.connections["udp"].add((ip.src, udp.sport, ip.dst, udp.dport))
        pcap_file.close()
            
    def icmp_classifier(self, verbose = False):
        self.clients["icmp"] = set()
        self.servers["icmp"] = set()
        self.connections["icmp"] = set()
        pcap_file = open(self.name)
        pcap = dpkt.pcap.Reader(pcap_file)
        for timestamp, buf in pcap:
            eth = dpkt.ethernet.Ethernet(buf)
            if eth.type != dpkt.ethernet.ETH_TYPE_IP:
                continue
            ip = eth.data
            if dpkt.ip.IP_PROTO_ICMP != ip.p:
                continue
            icmp = ip.data
            if not isinstance(icmp, dpkt.icmp.ICMP):
                continue
            if ip.src not in self.servers["icmp"]:
                self.clients["icmp"].add(ip.src) 
            if ip.dst not in self.clients["icmp"]:
                self.servers["icmp"].add(ip.dst)
            self.connections["icmp"].add((ip.src, 0, ip.dst, 0))
        pcap_file.close()

    def ip_classifier(self, verbose = False):
        self.clients["icmp"] = set()
        self.servers["icmp"] = set()
        self.connections["icmp"] = set()
        self.clients["udp"] = set()
        self.servers["udp"] = set()
        self.connections["udp"] = set()
        self.clients["tcp"] = set()
        self.servers["tcp"] = set()
        self.connections["tcp"] = set()
        pcap_file = open(self.name)
        pcap = dpkt.pcap.Reader(pcap_file)
        for timestamp, buf in pcap:
            eth = dpkt.ethernet.Ethernet(buf)
            if eth.type != dpkt.ethernet.ETH_TYPE_IP:
                continue
            ip = eth.data
            if dpkt.ip.IP_PROTO_ICMP == ip.p:
                icmp = ip.data
                if not isinstance(icmp, dpkt.icmp.ICMP):
                    continue
                if ip.src not in self.servers["icmp"]:
                    self.clients["icmp"].add(ip.src) 
                if ip.dst not in self.clients["icmp"]:
                    self.servers["icmp"].add(ip.dst)
                self.connections["icmp"].add((ip.src, 0, ip.dst, 0))
            elif dpkt.ip.IP_PROTO_UDP == ip.p:
                 udp = ip.data
                 if not isinstance(udp, dpkt.udp.UDP):
                     continue
                 if udp.sport >= 1024 :
                    if ip.src not in self.servers["udp"]:
                        self.clients["udp"].add(ip.src)
                 else:
                    if ip.src not in self.clients["udp"]:
                        self.servers["udp"].add(ip.src)
                 if udp.dport >= 1024:
                    if ip.dst not in self.servers["udp"]:
                        self.clients["udp"].add(ip.dst)
                 else:
                    if ip.dst not in self.clients["udp"]:
                        self.servers["udp"].add(ip.dst)
                 self.connections["udp"].add((ip.src, udp.sport, ip.dst, udp.dport))
            elif dpkt.ip.IP_PROTO_TCP == ip.p:
                tcp = ip.data
                if not isinstance(tcp, dpkt.tcp.TCP):
                    continue 
                if tcp.sport >= 1024:
                    if ip.src not in self.servers["tcp"]:
                        self.clients["tcp"].add(ip.src)
                else:
                    if ip.src not in self.clients["tcp"]:
                        self.servers["tcp"].add(ip.src) 
                if tcp.dport >= 1024:
                    if ip.dst not in self.servers["tcp"]:
                        self.clients["tcp"].add(ip.dst)
                else:
                    if ip.dst not in self.clients["tcp"]:
                        self.servers["tcp"].add(ip.dst)
                self.connections["tcp"].add((ip.src, tcp.sport, ip.dst, tcp.dport))
        pcap_file.close()

    def get_connections(self):
        ret = {}
        ret["ipv4"] = set()
        if self.connections["icmp"] != None:
            ret["icmp"] = self.connections["icmp"]
            ret["ipv4"] |= self.connections["icmp"]
        if self.connections["tcp"] != None:
            ret["tcp"] = self.connections["tcp"]
            ret["ipv4"] |= self.connections["tcp"]
        if self.connections["udp"] != None:
            ret["udp"] = self.connections["udp"]
            ret["ipv4"] |= self.connections["udp"]
        if 0 ==  len(ret["ipv4"]):
            return {}
        else:
            return ret

    def get_clients(self):
        ret = {}
        ret["ipv4"] = set()
        if self.clients["icmp"] != None:
            ret["icmp"] = self.clients["icmp"]
            ret["ipv4"] |= self.clients["icmp"]
        if self.clients["tcp"] != None:
            ret["tcp"] = self.clients["tcp"]
            ret["ipv4"] |= self.clients["tcp"]
        if self.clients["udp"] != None:
            ret["udp"] = self.clients["udp"]
            ret["ipv4"] |= self.clients["udp"]
        if 0 ==  len(ret["ipv4"]):
            return {}
        else:
            return ret

    def get_servers(self):
        ret = {}
        ret["ipv4"] = set()
        if self.servers["icmp"] != None:
            ret["icmp"] = self.servers["icmp"]
            ret["ipv4"] |= self.servers["icmp"]
        if self.servers["tcp"] != None:
            ret["tcp"] = self.servers["tcp"]
            ret["ipv4"] |= self.servers["tcp"]
        if self.servers["udp"] != None:
            ret["udp"] = self.servers["udp"]
            ret["ipv4"] |= self.servers["udp"]
        if 0 ==  len(ret["ipv4"]):
            return {}
        else:
            return ret

    def get_infos(self):
        ret = {}
        ret["connections"] = self.get_connections()
        ret["clients"] = self.get_clients()
        ret["servers"] = self.get_servers()
        ret["name"] = self.name 
        return ret

    def dump_infos(self):
        infos = self.get_infos()
        print "="*80
        print "Connections:"
        print "-"*80
        conn = infos["connections"]
        print "IPv4:",
        if "ipv4" in conn:
            print "    %ld" % len(conn["ipv4"])
        else: 
            print "    0"
        print "TCP :",
        if "tcp" in conn:
            print "    %ld" % len(conn["tcp"])
        else: 
            print "    0"
        print "UDP :",
        if "udp" in conn:
            print "    %ld" % len(conn["udp"])
        else: 
            print "    0"
        print "ICMP:",
        if "icmp" in conn:
            print "    %ld" % len(conn["icmp"])
        else: 
            print "    0"
        print "-"*80
        print "Clients:"
        print "-"*80
        conn = infos["clients"]
        print "IPv4:",
        if "ipv4" in conn:
            print "    %ld" % len(conn["ipv4"])
        else: 
            print "    0"
        print "TCP :",
        if "tcp" in conn:
            print "    %ld" % len(conn["tcp"])
        else: 
            print "    0"
        print "UDP :",
        if "udp" in conn:
            print "    %ld" % len(conn["udp"])
        else: 
            print "    0"
        print "ICMP:",
        if "icmp" in conn:
            print "    %ld" % len(conn["icmp"])
        else: 
            print "    0"
        print "-"*80
        print "Servers:"
        print "-"*80
        conn = infos["servers"]
        print "IPv4:", 
        if "ipv4" in conn:
            print "    %ld" % len(conn["ipv4"])
        else: 
            print "    0"
        print "TCP :", 
        if "tcp" in conn:
            print "    %ld" % len(conn["tcp"])
        else: 
            print "    0"
        print "UDP :", 
        if "udp" in conn:
            print "    %ld" % len(conn["udp"])
        else: 
            print "    0"
        print "ICMP:", 
        if "icmp" in conn:
            print "    %ld" % len(conn["icmp"])
        else: 
            print "    0"
        print "="*80

    def dump_clients(self):
        for protocol, hosts in self.clients.items():
            for host in hosts:
                print "%s_client:%ls" % (protocol, inet_to_str(host))
    
    def dump_servers(self):
        for protocol, hosts in self.servers.items():
            for host in hosts:
                print "%s_server:%ls" % (protocol, inet_to_str(host))

if __name__ == "__main__":
    file_name = sys.argv[1]
    test = capfile(file_name)
    test.ip_classifier()
    test.dump_clients()
    test.dump_servers()
    #test.dump_infos()    
    #test.tcp_classifier()
    #print "Clients: %ld" % len(test.clients["tcp"])
    #print "Servers: %ld" % len(test.servers["tcp"])
    #test.udp_classifier()
    #print "Clients: %ld" % len(test.clients["udp"])
    #print "Servers: %ld" % len(test.servers["udp"])
    #test.icmp_classifier()
    #print "Clients: %ld" % len(test.clients["icmp"])
    #print "Servers: %ld" % len(test.servers["icmp"])

