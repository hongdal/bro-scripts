#!/bin/bash
set -eu

# scripts folder is $1
if [ $# -lt 1 ]
then
    echo 'Usage: '$0' scripts_folder'
    exit 1
fi

./analyze.sh $1 | python events.py
