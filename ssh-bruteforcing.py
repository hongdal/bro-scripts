#!/usr/bin/env python3
# coding=utf-8
#----------------------------------------------------------------
# This script demonstrates: 
#   1) SSH connection under Python. 
#   2) Interactively input by using 'raw_input'. 
#   3) Interactively input password using 'getpass.getpass("prompt")' 
#   4) send a command via SSH connection to the remote machine.
#   5) Get results via SSH connection from the remote machine.
#
#   Issue: does not work on python3. pxssh not found!
#
# Author: shashibici 
#----------------------------------------------------------------
from pexpect import pxssh
import sys
import time


def one_ssh(hostname):
    try:                                                            
        # create an SSH
        s = pxssh.pxssh()
        # Ask for user input.
        username = "hongda"
        # password input. 
        password = "mmk"
        s.login (hostname, username, password)
        s.logout()
    except pxssh.ExceptionPxssh as e:
        print("pxssh failed on login.")

if __name__=='__main__':
    if len(sys.argv) < 3:
        print("Usage: "+sys.argv[0]+" <count> <hostip> ")
        exit()
    count = int(sys.argv[1])
    while count > 0:
        one_ssh(sys.argv[2])
        count -= 1
        time.sleep(2)



