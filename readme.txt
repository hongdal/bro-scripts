This set of scripts require:
    - apt-get install wireshark --> handful of pcaputility
    - apt-get install tcpreplay --> handful of tcputility
    - apt-get install unzip --> use to unzip v2.3.2.zip


Install scapy:
    python2 ./setup.py install


Print information about capture files: 
    - capinfos 

Display how many IP addresses in the file:
    - tshark
        E.g., tshark -r <input.pcap> -T fields -e ip.dst -e ip.src \
                -E separator=, | sort | uniq | wc -l

Modify timestamp of each pacekt in the file:
    - editcap
        E.g., editcap -t -3600 <infile.pcap> <outfile.pcap>





