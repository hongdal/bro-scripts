import sys
import json

def analyze(f):

    event_script_dict = {}

    for line in f.readlines():
        line = line.strip()
        if not line:
            continue

        line = line.split(',')
        if len(line) < 2:
            continue

        script = line[0]
        for event in line[1:]:
            if event_script_dict.get(event) is None:
                event_script_dict[event] = [script]
            else:
                event_script_dict[event].append(script)
    return event_script_dict

def scripts_count(event_script_dict):
    fmt = '%35s\t%s'
    counter = {}
    for k, v in event_script_dict.items():
        counter[k] = len(v)
    counter = counter.items()
    c_cmp = lambda x, y: 1 if x[1] > y[1] else 0 if x[1] == y[1] else -1
    counter.sort(c_cmp)
    once = 0
    for k, v in counter:
        if v is 1:
            once += 1
        print fmt %(k, v)
    print '-'*79
    print 'Summary:'
    print fmt %('event that occurs once', once)
    print fmt %('Total events', len(counter))


if __name__ == '__main__':
    if len(sys.argv) > 1:
        f = open(sys.argv[1])
    else:
        f = sys.stdin
    res = analyze(f)

    f.close()

    scripts_count(res)
