#!/usr/bin/env python
# coding=utf-8

from scapy.all import *
from scapy.utils import rdpcap

sendp(rdpcap("outside.tcpdump"), iface="eth2")  # could be used like this rdpcap("filename",500) fetches first 500 pkts

exit(1)
new_dst_mac="00:8c:fa:5b:0d:8a"
new_src_mac="00:8c:fa:5a:fa:94"
for pkt in pkts:
    pkt[Ether].src= new_src_mac  # i.e new_src_mac="00:11:22:33:44:55"
    pkt[Ether].dst= new_dst_mac
#    pkt[IP].src= new_src_ip # i.e new_src_ip="255.255.255.255"
#    pkt[IP].dst= new_dst_ip
    sendp(pkt, iface="eth2") #sending packet at layer 2
