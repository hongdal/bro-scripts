#!/bin/bash
# -----------------------------------------------------------------------------
# Note: Take your risk!
# 
# -----------------------------------------------------------------------------


if [[ -z $2 ]]; then 
    echo "Usage: $0 <on|off> <interface>"
    echo "E.g., $0 on eth2"
    exit 1
fi

if [[ $1 == 'on' ]]; then
    ethtool --offload $2 rx on tx on 
    if [[ $? -ne 0 ]]; then 
        echo "ethtool --offload $2 rx on tx on : failed! " 1>&2
        exit 1
    fi
    ethtool -K $2 gso on gro on 
    if [[ $? -ne 0 ]]; then 
        echo "ethtool -K $2 gso on gro on : failed! " 1>&2
        exit 1
    fi
elif [[ $1 == 'off' ]]; then 
    ethtool --offload $2 rx off tx off
    if [[ $? -ne 0 ]]; then 
        echo "ethtool --offload $2 rx off tx off : failed! " 1>&2
        exit 1
    fi
    ethtool -K $2 gso off gro off 
    if [[ $? -ne 0 ]]; then 
        echo "ethtool -K $2 gso off gro off : failed! " 1>&2
        exit 1
    fi
else
    echo "Usage: $0 <on|off> <interface>"
    echo "E.g., $0 on eth2"
    exit 1
fi

ethtool --show-offload $2

