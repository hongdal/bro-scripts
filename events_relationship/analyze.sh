#!/bin/bash
set -e

folder=$1

if [ ! -d $folder ]
then
    echo 'There is no bro scripts under the scripts folder' > /dev/stderr
    exit 1
fi

scripts=$(find $folder -name '*.bro')
echo 'a' $scripts
for script in $scripts
do
    events=$(grep '^event .*(' $script | awk -F '(' '{split($1, a, " "); print ","a[2]}' | tr -d '\n')
    if [ ! -z $events ]
    then
        echo $script$events
    fi
done
