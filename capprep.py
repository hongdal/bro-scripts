#!/usr/bin/env python2
# coding=utf-8

import sys
from capfile import capfile

class capprep(object):

    def __init__(self):
        # { name => capfile objects }
        self.capfiles = {}

    def add_capfile(self, filename):
        temp = capfile(filename)
        self.capfiles[temp.name] = temp

    def dump_infos_by_name(self, name):
        if name in self.capfiles:
            self.capfiles[name].dump_infos()
    def ip_classify_by_name(self, name):
        if name in self.capfiles:
            self.capfiles[name].ip_classifier()


if __name__ == "__main__":
    test = capprep()
    filename = sys.argv[1]
    test.add_capfile(filename)
    capfiles = test.capfiles
    for name in capfiles.keys():
        test.ip_classify_by_name(name)
        test.dump_infos_by_name(name)

